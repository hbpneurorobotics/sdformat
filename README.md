This repository is part of the Neurorobotics Platform software
Copyright (C) Human Brain Project
https://neurorobotics.net

The Human Brain Project is a European Commission funded project
in the frame of the [Horizon2020 FET Flagship plan](http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships).

This work has received funding from the European Union’s Horizon 2020 Framework Programme for Research and Innovation under the Specific Grant Agreement No. 720270 (Human Brain Project SGA1), and the Specific Grant Agreement No. 785907 (Human Brain Project SGA2), and under the Specific Grant Agreement No. 945539 (Human Brain Project SGA3).


You are free to clone this repository and amend its code with respect to
the license that you find in the root folder.

## Submitting a pull request

To submit code changes (pull requests) as a person external to the project, do as follows.

0. Log in to Bitbucket
1. Fork the project: "+" button, "Fork this repository".
2. Clone the forked project (eg. ```git clone git@bitbucket.org:[USERNAME]/[REPOSITORY].git```)
3. Create a branch. Give it an explicit name referring a ticket number in [Jira](https://hbpneurorobotics.atlassian.net)
4. Do your code changes
5. Commit your changes. 
  **Make sure** your commit message starts with ```[<ticket_number>]```.
   Example: "[NUIT-10] My new feature"
6. ```git push```
7. Click on the url provided on the console output for the previous command to create the pull request
8. A core developer will eventually review your pull request and approve or comment it


# sdformat #

SDFormat is an XML file format that describes environments, objects, and robots
in a manner suitable for robotic applications. SDFormat is capable of representing
and describing different physic engines, lighting properties, terrain, static
or dynamic objects, and articulated robots with various sensors, and acutators.
The format of SDFormat is also described by XML, which facilitates updates and
allows conversion from previous versions.

* SDFormat - The specification.
    * SDF - Synonym for SDFormat, though SDFormat should be preferred, as "SDF"
      is an acronym with other meanings.
* libsdformat - The C++ parsing code contained within this repository,
  which can be used to read SDFormat files and return a C++ interface.

Test coverage:

[![codecov](https://codecov.io/bb/osrf/sdformat/branch/default/graph/badge.svg)](https://codecov.io/bb/osrf/sdformat)


## Installation ##

Standard installation can be performed in UNIX systems using the following
steps:

 - mkdir build/
 - cd build/
 - cmake ..
 - sudo make install

sdformat supported cmake parameters at configuring time:
 - USE_INTERNAL_URDF (bool) [default False]
   Use an internal copy of urdfdom 1.0.0 instead of look for one
   installed in the system
 - USE_UPSTREAM_CFLAGS (bool) [default True]
   Use the sdformat team compilation flags instead of the common set defined
   by cmake.

## Uninstallation ##

To uninstall the software installed with the previous steps:
 - cd build/
 - sudo make uninstall
